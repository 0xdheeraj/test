## Problems Faced so far

- In-house Lunch / Food coupons like Zeta (helps in Tax Savings)
- Seminars related to financial management ex: Tax Savings
- No proper documentation on Product onboarding and flows.
- No proper designs ex: Some things are on Figma while others on XD. Also, they are not properly versioned so it becomes nearly impossible to distinguish between the new and old flows.
- Sprint should be of 2 weeks.
- API documentation is not proper ex: Frontend devs don't have idea on available includes and other query params.
- Ownership of Internal tools for API changes. 
- Devs need monitors for better productivity
- QA gets less time for testing as builds keeps coming in frequently. It decreases time for each build.
- No Parties on major deployments
- No proper mechanism for API Testing and Load Testing
- Need more PS games as there's very limited availability
- No proper standard defined for APIs Dev
- There's no standardised platform for Development, Issue Tracking, etc. Ex: Issues are being reported on Notion, WhatsApp, etc.
- Slack is still not upgraded due to which we're unable to access older messages and threads
- Need better version control platform. Github Preferred
- Urban Vault people keeps disturbing. Ex: The construction work should be done on weekends.
- Team Dinner/Outings once a month
- Competitions based on internal games that we have available.

## Development that needs to happen in the next 3 months

### Tech debt

Mobile:
- Unit Testing (Repositories) on Student Side
- Student Onboarding Video
- OTP Autofill
- Debugging Devices
- Sentry

Web (Frontend):
- Incomplete dashboard functionalities(open/scheduled/chcecking pending)
- Test feature implementation on teachers web app.
- Modification/cleanup existing code for better user experience
- Events SDK (GA) integration
- Student side Web App at par with mobile app

Backend:
- Microservices architecture implementation
- Migrate PHP services to Newer tech stack
- Elastic Search implementation for searchable items

QA:
- Devices for Testing

Internal Tools:
- Mega Olympiad
- Quest Creation
- ACL (Access Control List)
- Admin panel migration to Portal

### Performance Optimisations

Mobile:
- API calls optimisations ex: includes
- Proper Architecture to make future dev scalable

Backend:
- Moving out existing workers to new tech stack
- Moving out existing workers to different instance on prod
- Cache Implementation
- Performance improvements for high throughput APIs
- Teacher Group level Analytics redesigning
- Dashboard improvements by reducing/clubbing API calls

Web (Frontend):
- Minify CSS and JS
- API optimization for better speed and fluidity

QA:
- Automation
- Test Lab - POC

Internal Tools:
- Admin APIs DB query optimisations

### Product Enhancements

- Shimmers
- Engaging Loaders
- Teacher Banners
- Video Tutorials on Banners/Help section
- Early access to challenges based on XPs
- Email Verification
- OTP/Password less Authentication -> True Caller
- Student My Questts: Slide to Switch Tabs
- Student My Questts: Test and Homework Separation
- Backend level control for empty data handling. Ex: Don't send chapters with 0 Questts
- Dashboard Challenges needs to be arranged in a proper/better manner or we can go with a separate page for challenges itself.
- Some sort of Indication for maximum profiles while onboarding or profile creation
- SSO (Single Sign On)
- Redesign/Improve UI and UX for teacher profile
- Existing Fun Questt content needs to be improved
- Some sort of appreciation gesture if my rank is in top 3 or top 10 like celebration animations / Flying balloons

### Product feature suggestions

- Feed to brag about achievements
- Social sharing option with templating
- Discussion Forums on different levels ex: Group, Grade, School, etc
- Ask for interests along with Grade while onboarding student, so that the dashboard gets populated with engaging content. Ex: Any Music Streaming App
- Proper Implementation for Refer-and-earn feature
- There's no usage of XPs. For example I can buy some study items through XPs.
- Side menu for Quick navigation to features that requires a lot of taps

## Processes changes required in the next 3 months

- Testing by whole team for Major releases. 
- RCAs (Root Cause Analysis) for Bugs. Without RCAs, we'll not be moving forward with Bug resolution
- CI/CD 
- QA resources needed for Internal tools
